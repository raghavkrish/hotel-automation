require './building/sub_corridor'

RSpec.describe SubCorridor do

  it 'Creates Main Corridor object' do
    sc = SubCorridor.new
    expect(sc).to be_truthy
  end

  it 'Checks default state of ac' do
    sc = SubCorridor.new
    expect(sc.ac.state).to eq('ON')
  end

  it 'Checks default state of light' do
    sc = SubCorridor.new
    expect(sc.light.state).to eq('OFF')
  end

  it 'checking the power consumption of main_corridor' do
    sc = SubCorridor.new
    expect(sc.total_power).to eq(10)
  end

end