require './building/corridor'


RSpec.describe Corridor do

  it 'Creates corridor object' do
    c = Corridor.new
    expect(c).to be_truthy
    expect(c.ac).to be_truthy
    expect(c.light).to be_truthy
  end

end