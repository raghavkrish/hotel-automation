require './appliances/appliance'

RSpec.describe Appliance do

  it 'turns off the appliance' do
    a = Appliance.new
    a.turn_off
    expect(a.state).to eq('OFF')
  end
  
  it 'turns on the appliance' do
    a = Appliance.new
    a.turn_on
    expect(a.state).to eq('ON')
  end

end
