require './appliances/air_conditioner'


RSpec.describe AirConditioner do

  it 'Air Conditioner ON by defult' do
    a = AirConditioner.new
    expect(a.state).to eq('ON')
  end

  it 'turns OFF the Air Conditioner' do
    a = AirConditioner.new
    a.turn_off
    expect(a.state).to eq('OFF')
  end
  
  it 'turns ON the Air Conditioner' do
    a = AirConditioner.new
    a.turn_off
    a.turn_on
    expect(a.state).to eq('ON')
  end

  it 'Checks the power consumed by the Air Conditioner' do
    a = AirConditioner.new
    expect(a.power_consumption).to eq(10)
  end

end