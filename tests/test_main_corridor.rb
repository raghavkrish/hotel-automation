require './building/main_corridor'

RSpec.describe MainCorridor do

  it 'Creates Main Corridor object' do
    mc = MainCorridor.new
    expect(mc).to be_truthy
  end

  it 'Checks default state of ac' do
    mc = MainCorridor.new
    expect(mc.ac.state).to eq('ON')
  end

  it 'Checks default state of light' do
    mc = MainCorridor.new
    expect(mc.light.state).to eq('ON')
  end

  it 'checking the power consumption of main_corridor' do
    mc = MainCorridor.new
    expect(mc.total_power).to eq(15)
  end

end