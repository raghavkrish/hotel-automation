require './building/floor'

RSpec.describe Floor do
  
  it 'creats a Floor with main and sub corridors' do
    MAIN_CORRIDORS = 1
    SUB_CORRIDORS = 2
    f = Floor.new(MAIN_CORRIDORS, SUB_CORRIDORS)
    expect(f.main_corridors.length).to eq(MAIN_CORRIDORS)
    expect(f.sub_corridors.length).to eq(SUB_CORRIDORS)
  end
end