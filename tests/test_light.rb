require './appliances/light'


RSpec.describe Light do

  it 'Light nil by defult' do
    l = Light.new
    expect(l.state).to eq(nil)
  end

  it 'turns OFF the Light' do
    l = Light.new
    l.turn_off
    expect(l.state).to eq('OFF')
  end
  
  it 'turns ON the Light' do
    l = Light.new
    l.turn_on
    expect(l.state).to eq('ON')
  end

  it 'Checks the power consumed by the Light' do
    l = Light.new
    expect(l.power_consumption).to eq(5)
  end

end