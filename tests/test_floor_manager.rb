require './building/floor_manager'

RSpec.describe FloorManager do
  
  it 'handles movement' do
    f = Floor.new(MAIN_CORRIDORS, SUB_CORRIDORS)
    fm = FloorManager.new
    f.sub_corridors.each do |sc|
      expect(sc.light.state).to eq("OFF")
    end
    f.sub_corridors.each do |sc|
      expect(sc.ac.state).to eq("ON")
    end
    fm.handle_movement(f, 2)
    expect(f.sub_corridors.first.ac.state).to eq("OFF")
    expect(f.sub_corridors.last.light.state).to eq("ON")
  end

  it 'handles no movement' do
    f = Floor.new(MAIN_CORRIDORS, SUB_CORRIDORS)
    fm = FloorManager.new
    f.sub_corridors.each do |sc|
      expect(sc.light.state).to eq("OFF")
    end
    f.sub_corridors.each do |sc|
      expect(sc.ac.state).to eq("ON")
    end
    fm.handle_movement(f, 2)
    expect(f.sub_corridors.first.ac.state).to eq("OFF")
    expect(f.sub_corridors.last.light.state).to eq("ON")
    fm.handle_no_movement(f, 2)
    f.sub_corridors.each do |sc|
      expect(sc.light.state).to eq("OFF")
    end
    f.sub_corridors.each do |sc|
      expect(sc.ac.state).to eq("ON")
    end
  end

  it 'tests max power consumption' do
    expected = 35
    fm = FloorManager.new
    f = Floor.new(MAIN_CORRIDORS, SUB_CORRIDORS)
    expect(fm.max_power_consumption(f)).to eq(expected)
  end

  it 'calculates intantaneous power consumption' do
    expected = 35
    fm = FloorManager.new
    f = Floor.new(MAIN_CORRIDORS, SUB_CORRIDORS)
    expect(fm.floor_power_consumption(f)).to eq(expected)
  end

end