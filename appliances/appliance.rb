class Appliance

  def initialize
    @state
    @power
  end

  def turn_on
    @state = "ON"
  end

  def turn_off
    @state = "OFF"
  end

  def get_state
    @state
  end

  def power
    @power
  end

end
