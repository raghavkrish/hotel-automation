class Appliance

  ON_STATE = 'ON'
  OFF_STATE = 'OFF'
  
  def initialize
    @state
    @power
  end

  def turn_on
    @state = ON_STATE
  end

  def turn_off
    @state = OFF_STATE
  end

  def on?
    @state == ON_STATE
  end

  def power_usage
    on? ? @power : 0
  end

  def state
    @state
  end

  def power_consumption
    @power
  end

end
