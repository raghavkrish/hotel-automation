module PowerConsumption

  def max_power_consumption mc, sc
    (mc*15) + (sc*10)
  end

  def calculate_power_consumption mcs, scs
    
    mcspower = 0
    mcs.each do |mc|
      mcspower += mc.total_power
    end

    scspower = 0
    scs.each do |sc|
      scspower += sc.total_power
    end
    mcspower + scspower
  end

end
