require "./appliances/appliance"

class AirConditioner < Appliance

  AC_POWER_CONSUMPTION = 10
  
  def initialize
    super
    turn_on
    @power = AC_POWER_CONSUMPTION
  end
end
