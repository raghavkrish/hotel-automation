require "./appliances/appliance"

class AirConditioner < Appliance
  def initialize
    super
    turn_on
    @power = 10
  end
end
