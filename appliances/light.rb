require './appliances/appliance'
require 'byebug'

class Light < Appliance
  def initialize
    super
    @power = 5
  end
end
