require './appliances/appliance'

class Light < Appliance

  LIGHT_POWER_CONSUMPTION = 5

  def initialize
    super
    @power = LIGHT_POWER_CONSUMPTION
  end
end
