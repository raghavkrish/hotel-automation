require './building/corridor'

class SubCorridor < Corridor

  def initialize
    super
    @light.turn_off
  end

  def total_power
    power_consumed = 0
    power_consumed += get_light.power if get_light.get_state == "ON"
    power_consumed += get_ac.power if get_ac.get_state == "ON"
    power_consumed
  end

end
