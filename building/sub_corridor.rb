require './building/corridor'

class SubCorridor < Corridor
  def initialize
    super
    light.turn_off
  end
end
