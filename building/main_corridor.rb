require './building/corridor'

class MainCorridor < Corridor
  def initialize
    super
    light.turn_on
  end
end
