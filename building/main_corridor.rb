require './building/corridor'

class MainCorridor < Corridor

  def initialize
    super
    @light.turn_on
  end

  def total_power
    @light.power + @ac.power
  end

end
