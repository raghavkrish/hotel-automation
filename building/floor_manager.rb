require './building/floor'

class FloorManager

  def handle_movement floor, sc_index
    return unless toggle_and_check_power(floor, sc_index)

    corridors = get_other_corridors floor, sc_index
    corridors.each do |corridor|
      corridor.ac.turn_off if corridor.ac.on?
      break unless power_consumption_exceeded floor
    end
  end

  def handle_no_movement floor, sc_index
    return if toggle_and_check_power(floor, sc_index)

    corridors = get_other_corridors floor, sc_index
    corridors.each do |corridor|
      break unless power_not_exceeds floor, corridor.ac
      corridor.ac.turn_on if !corridor.ac.on?
    end
  end

  def power_not_exceeds floor, appliance
    floor_power_consumption(floor) + appliance.power_consumption <= max_power_consumption(floor)
  end

  def max_power_consumption floor
    mc = floor.main_corridors.length
    sc = floor.sub_corridors.length
    
    (mc*15) + (sc*10)
  end

  def get_other_corridors floor, sc_index
    sub_corridor = floor.get_sub_corridor(sc_index)
    floor.sub_corridors - [sub_corridor]
  end

  def toggle_and_check_power floor, sc_index
    sub_corridor = floor.get_sub_corridor(sc_index)
    toggle_appliance(sub_corridor.light)
    power_consumption_exceeded floor
  end

  def floor_power_consumption floor
    mcs = floor.main_corridors
    scs = floor.sub_corridors
      
    mcspower = mcs.map(&:total_power).inject(0, &:+)
    scspower = scs.map(&:total_power).inject(0, &:+)
    mcspower + scspower
  end

  def toggle_appliance appliance
    appliance.on? ? appliance.turn_off : appliance.turn_on
  end

  def power_consumption_exceeded floor
    power_consumed = floor_power_consumption floor
    power_consumed > max_power_consumption(floor)
  end

end
