require './building/floor'

class Hotel

  def initialize floor_count, main_corridor_count, sub_corridor_count
    @floors = []
    floor_count.times do
      @floors.push(Floor.new(main_corridor_count, sub_corridor_count))
    end
  end

  def display_status
    @floors.each_with_index do |floor, index|
      puts "\t\tFloor #{index + 1}"
      floor.get_main_corridors.each_with_index do |mc, index1|
        puts "Main Corridor #{index1 + 1} Light: #{mc.get_light.get_state} AC : #{mc.get_ac.get_state}"
      end
      floor.get_sub_corridors.each_with_index do |mc, index1|
        puts "Sub Corridor #{index1 + 1} Light: #{mc.get_light.get_state} AC : #{mc.get_ac.get_state}"
      end
    end
  end

  def get_floor index
    @floors[index-1]
  end
end
