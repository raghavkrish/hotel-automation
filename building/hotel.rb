require './building/floor'
require './building/floor_manager'

class Hotel

  def initialize floor_count, main_corridor_count, sub_corridor_count
    @floors = []
    floor_count.times do
      @floors.push(Floor.new(main_corridor_count, sub_corridor_count))
    end
    @fm = FloorManager.new
  end

  def display_status
    @floors.each_with_index do |floor, index|
      puts "\n\t\tFloor #{index + 1}"
      floor.main_corridors.each_with_index do |mc, index1|
        puts "Main Corridor #{index1 + 1} Light: #{mc.light.state} AC : #{mc.ac.state}"
      end
      floor.sub_corridors.each_with_index do |sc, index1|
        puts "Sub Corridor #{index1 + 1} Light: #{sc.light.state} AC : #{sc.ac.state}"
      end
    end
  end

  def floors
    @floors
  end

  def floor_manager
    @fm
  end

  def get_floor index
    @floors[index-1]
  end
end
