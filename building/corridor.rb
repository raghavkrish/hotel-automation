require './appliances/light'
require './appliances/air_conditioner'

class Corridor

  def initialize
    @ac = AirConditioner.new
    @light = Light.new
    @power
  end

  def get_light
    @light
  end

  def get_ac
    @ac
  end
end
