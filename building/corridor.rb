require './appliances/light'
require './appliances/air_conditioner'

class Corridor

  def initialize
    @appliances = [Light.new, AirConditioner.new]
  end

  def light
    @appliances.first
  end

  def ac
    @appliances.last
  end

  def total_power
    pwr = 0
    @appliances.each do |app|
      pwr += app.power_usage
    end
    pwr
  end
end
