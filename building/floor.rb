require './building/main_corridor'
require './building/sub_corridor'

class Floor

  def initialize main_corridor_count, sub_corridor_count
    @main_corridors = []
    @sub_corridors = []
    create_main_corridors main_corridor_count
    create_sub_corridors sub_corridor_count
  end

  def main_corridors
    @main_corridors
  end

  def sub_corridors
    @sub_corridors
  end

  def get_sub_corridor index
    @sub_corridors[index-1]
  end

  private
    def create_main_corridors count
      count.times  do
        @main_corridors.push(MainCorridor.new)
      end
    end

    def create_sub_corridors count
      count.times do
        @sub_corridors.push(SubCorridor.new)
      end
    end
end
