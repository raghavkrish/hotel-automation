require './building/main_corridor'
require './building/sub_corridor'
require './appliances/power_consumption'

class Floor
  include PowerConsumption

  def initialize main_corridor_count, sub_corridor_count
    @main_corridors = []
    @sub_corridors = []
    create_main_corridors main_corridor_count
    create_sub_corridors sub_corridor_count
    @power_consumption = calculate_power_consumption @main_corridors, @sub_corridors
  end

  def create_main_corridors count
    count.times  do
      @main_corridors.push(MainCorridor.new)
    end
  end

  def create_sub_corridors count
    count.times do
      @sub_corridors.push(SubCorridor.new)
    end
  end

  def handle_movement sc
    subc = get_sub_corridor(sc)
    subc.get_light.turn_on if subc.get_light.get_state == 'OFF'
    new_power_consumption = calculate_power_consumption @main_corridors, @sub_corridors
    if new_power_consumption > max_power_consumption(@main_corridors.length, @sub_corridors.length)
      @sub_corridors.each_with_index do |wtf, index|
        next if index-1 == sc
        wtf.get_ac.turn_off if wtf.get_ac.get_state == "ON"
        int_power_consumption = calculate_power_consumption @main_corridors, @sub_corridors
        break if int_power_consumption <= max_power_consumption(@main_corridors.length, @sub_corridors.length)
      end
    end
  end

  def handle_no_movement sc
    subc = get_sub_corridor(sc)
    subc.get_light.turn_off if subc.get_light.get_state == 'ON'
    new_power_consumption = calculate_power_consumption @main_corridors, @sub_corridors
    @sub_corridors.each_with_index do |wtf, index|
      next if index-1 == sc
      wtf.get_ac.turn_on if (new_power_consumption + wtf.get_ac.power) <= max_power_consumption(@main_corridors.length, @sub_corridors.length)
    end
  end

  def get_main_corridors
    @main_corridors
  end

  def get_sub_corridors
    @sub_corridors
  end

  def get_sub_corridor index
    @sub_corridors[index-1]
  end
end
