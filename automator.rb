require './building/hotel'

class Automator
  def initialize
    @hotel = Hotel.new(2,1,2)
    check_movements
  end

  def check_movements
    @hotel.display_status
    mimic_movement(1,2)
    @hotel.display_status
    no_movement(1,2)
    @hotel.display_status
  end

  def mimic_movement floor, sub_corridor
    @hotel.get_floor(floor).handle_movement(sub_corridor)
  end

  def no_movement floor, sub_corridor
    @hotel.get_floor(floor).handle_no_movement(sub_corridor)
  end
end

Automator.new