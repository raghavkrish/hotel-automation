require './building/hotel'
require './input'

class Automator
  def initialize
    @hotel = Hotel.new(NO_FLOORS,NO_MAIN_CORR,NO_SUB_CORR)
    mimic_events
  end

  def mimic_events
    @hotel.display_status
    MOVEMENTS.each do |movement|
      if movement.first == 'M'
        mimic_movement(movement[1], movement[2])
      elsif movement.first == 'N'
        no_movement(movement[1], movement[2])
      end    
      @hotel.display_status
    end
  end

  def mimic_movement floor, sub_corridor
    @hotel.floor_manager.handle_movement(@hotel.get_floor(floor), sub_corridor)
  end

  def no_movement floor, sub_corridor
    @hotel.floor_manager.handle_no_movement(@hotel.get_floor(floor), sub_corridor)
  end
end

Automator.new